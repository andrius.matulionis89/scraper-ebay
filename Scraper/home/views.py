import re
from django.shortcuts import render

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from .models import Scrape
from django.urls import reverse


#@login_required(login_url="/login/")
def index(request):
    context = {'segment': 'index'}
    return render(request,'home/index.html', context)
  


#@login_required(login_url="/login/")
def tables_data(request):
    data = Scrape.objects.all()
    context = {'data':data}
    return render(request, 'home/tables-data.html', context)
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
