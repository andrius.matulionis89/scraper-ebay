from django.urls import path

from .views import index, tables_data

urlpatterns = [
    path('', index, name="home"),
    path('scraping-table/', tables_data, name="pages"),
]
