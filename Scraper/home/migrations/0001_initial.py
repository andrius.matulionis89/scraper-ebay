# Generated by Django 3.2.11 on 2022-05-28 14:00

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Scrape',
            fields=[
                ('id', models.BigAutoField(auto_created=False, primary_key=False, serialize=False, verbose_name='ID', null=True)),
                ('item', models.CharField(max_length=100)),
                ('price', models.FloatField(default=0.0)),
                ('location', models.CharField(max_length=100)),
                ('condition', models.CharField(max_length=50)),
                ('seller', models.CharField(max_length=50)),
                ('url', models.CharField(max_length=250)),
            ],
            options={
                'db_table': 'scraped',
            },
        ),
    ]
